/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */
package com.huawei.housekeeper.controller.request;

import lombok.Getter;
import lombok.Setter;

/**
 * 增加配置
 *
 * @author y00464350
 * @since 2022-03-24
 */
@Getter
@Setter
public class SetPropertyDto {
    private String key;

    private String value;

    private String application;

    private String profile;

    private String label;
}
