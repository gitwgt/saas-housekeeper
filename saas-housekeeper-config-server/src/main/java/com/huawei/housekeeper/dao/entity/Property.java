/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.entity;

import lombok.Data;

/**
 * 配置项
 *
 * @author y00464350
 * @since 2022-03-24
 */
@Data
public class Property {

    private String id;

    private String key;

    private String value;

    private String application;

    private String profile;

    private String label;

}
