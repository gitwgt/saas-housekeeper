/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.service;

import com.huawei.housekeeper.controller.request.UserLoginDto;

/**
 * 用户信息服务
 *
 * @author y00464350
 * @since 2022-02-08
 */
public interface UserLoginService {
    /**
     * 用户登录
     *
     * @param userLoginDto 用户登录信息
     * @return boolean
     */
    String userLogin(UserLoginDto userLoginDto);
}
