/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import com.huawei.housekeeper.request.PageRequest;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.hibernate.validator.constraints.Length;

/**
 * 用户分页查询对象
 *
 * @author l84165417
 * @since 2022/1/26 15:18
 */
@Data
@ApiModel("用户分页查询对象")
public class PageQueryUserDto extends PageRequest {
    @Length(max = 255, message = "最大长度:255")
    @ApiModelProperty(value = "用户名", required = false)
    private String userName;

    @ApiModelProperty(value = "用户角色", required = false)
    @TableField("USER_ROLE")
    private Integer userRole;
}