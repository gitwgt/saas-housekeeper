/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("根据serviceID查询所有的可选服务")
public class GetServiceSelectionByServiceIdVo {

    @ApiModelProperty("可选服务ID")
    private Long id;

    @ApiModelProperty("规格选项Id")
    private Long optionId;

    @ApiModelProperty("skuId")
    private Long skuId;
}