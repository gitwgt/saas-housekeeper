/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

CREATE TABLE `t_task`
(
    `id`               int                                                     NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `customer_id`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '顾客id;用户中心上下文',
    `order_id`         int                                                     NULL DEFAULT NULL COMMENT '订单id;订单中心上下文',
    `customer_name`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '顾客名称',
    `customer_phone`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码',
    `appointment_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务时间',
    `address`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地址',
    `service_name`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务名称;订单中心推送',
    `service_detail`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务细节;订单中心推送',
    `salary`           decimal(24, 6)                                          NULL DEFAULT NULL COMMENT '佣金,可通过支付金额得出',
    `employee_name`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '雇佣名称',
    `employee_id`      varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '雇佣id',
    `task_status`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务状态',
    `created_by`       varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '创建人',
    `created_time`     datetime                                                NULL DEFAULT NULL COMMENT '创建时间',
    `updated_by`       varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '更新人',
    `updated_time`     datetime                                                NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 207179779
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '任务表'
  ROW_FORMAT = COMPACT;