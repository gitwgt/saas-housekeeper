/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

alter table `t_order`
    modify column employee_id varchar(32);