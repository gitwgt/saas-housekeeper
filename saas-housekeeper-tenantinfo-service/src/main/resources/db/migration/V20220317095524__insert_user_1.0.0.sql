/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

# 新增租户账户
insert into `user` (user_id, user_name, `password`, user_role, email)
values ('597g0c1160e04a4680dcaf1656e97v35', 'tenant', '$2a$10$59mhs7XtfNgeIvZjJw6pKeJ6q7IXptiVlh5IUXYgD0eo0Iohw/.6q',
        3, '14640cg0@163.com');