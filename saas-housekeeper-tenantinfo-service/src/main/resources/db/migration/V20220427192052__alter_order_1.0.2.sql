ALTER TABLE `t_order`
    ADD order_time datetime comment '接单时间';

ALTER TABLE `t_order`
    ADD completion_time datetime comment '完成时间';