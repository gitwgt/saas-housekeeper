update `user`
set password = '$2a$10$V2e.ZgvIIJEJEF60uzUfUuFIOgfSV4b2UQXLDwECO21mSfZyz9JGC'
where user_name = 'tenant';