
package com.huawei.housekeeper.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * flyway迁移信息
 *
 * @author lWX1128557
 * @since 2022-04-27
 */
@Getter
@Setter
@ApiModel(value = "迁移详情")
public class GetMigrationInformationVo {
    @ApiModelProperty(value = "迁移总数")
    private Integer total;

    @ApiModelProperty(value = "迁移失败信息")
    private Map<String, String> failInfo;

    @ApiModelProperty(value = "迁移成功数")
    private Integer successful;

    @ApiModelProperty(value = "迁移失败数")
    private Integer failure;
}
