/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.convert;

import com.huawei.housekeeper.controller.request.CreateTenantDto;
import com.huawei.housekeeper.controller.response.GetTenantDetailVo;
import com.huawei.housekeeper.dao.entities.Tenant;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 租户对象映射
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
@Mapper(componentModel = "spring")
public interface TenantConvert {

    /**
     * mapstruct映射实例
     */
    TenantConvert INSTANCE = Mappers.getMapper(TenantConvert.class);

    /**
     * 创建租户Dto映射租户
     *
     * @param createTenantDto 创建租户Dto
     * @return Tenant 租户实体
     */
    @Mappings({@Mapping(target = "createdBy", source = "name"), @Mapping(target = "updatedBy", source = "name"),
        @Mapping(target = "status", constant = "1")})
    Tenant toTenant(CreateTenantDto createTenantDto);

    /**
     * 租户映射租户详情Vo
     *
     * @param tenant 租户实体
     * @return GetTenantDetailVo 租户详情Vo
     */
    GetTenantDetailVo toTenantDetailVo(Tenant tenant);

    /**
     * 租户列表映射租户详情Vo
     *
     * @param tenants 租户列表
     * @return GetTenantDetailVo 租户详情Vo
     */
    List<GetTenantDetailVo> toGetTenantDetailVos(List<Tenant> tenants);
}
