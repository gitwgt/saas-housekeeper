package com.huawei.housekeeper.dao.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huawei.housekeeper.dao.entities.TenantRegisterRecord;

/**
 * 功能描述
 *
 * @since 2022-10-20
 */
@Mapper
public interface RegisterRecordMapper extends BaseMapper<TenantRegisterRecord> {
}