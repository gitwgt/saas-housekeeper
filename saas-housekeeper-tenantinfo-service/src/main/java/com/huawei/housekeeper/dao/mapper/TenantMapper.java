/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.dao.mapper;

import com.huawei.housekeeper.dao.entities.Tenant;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * 租户mapper层
 *
 * @author lWX1128557
 * @since 2022-03-02
 */
@Mapper
public interface TenantMapper extends BaseMapper<Tenant> {
}
