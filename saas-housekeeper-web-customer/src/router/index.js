const router = [
    {
        path: '/',
        name: '/',
        component: () => import('@/views/Login/Login.vue'),
    },
    {
        path: '/login',
        name: '/login',
        component: () => import('@/views/Login/Login.vue'),
    },
    {
        path: '/register',
        name: '/register',
        component: () => import('@/views/Register/Register.vue'),
    },
    {
        path: '/home',
        name: '/home',
        component: () => import('@/views/Home/Home.vue'),
        children: [
            {
                path: '/home/service-list/service-details/:id',
                name: '/home/service-list/service-details',
                component: () => import('@/views/ServiceList/ServiceDetails.vue'),
            },
            {
                path: '/home/service-list',
                name: '/home/service-list',
                component: () => import('@/views/ServiceList/ServiceList.vue'),
            },
            {
                path: '/home/service-list/service-info',
                name: '/home/service-list/service-info',
                component: () => import('@/views/ServiceList/ServiceInfo.vue'),
            },
            {
                path: '/home/my-orders',
                name: '/home/my-orders',
                component: () => import('@/views/MyOrder/MyOrder.vue'),
            },
            {
                path: '/home/my-orders/details',
                name: '/home/my-orders/details',
                component: () => import('@/views/MyOrder/OrderDetails.vue'),
            },
        ],
    },
];

export default router;
