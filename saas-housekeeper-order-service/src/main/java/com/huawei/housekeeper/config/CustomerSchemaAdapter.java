
package com.huawei.housekeeper.config;

import com.huawei.saashousekeeper.config.binding.SchemaBindingStrategy;
import com.huawei.saashousekeeper.properties.TenantProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * schema适配器扩展
 *
 * @author lWX1156935
 * @since 2022-04-24
 */
@Configuration
public class CustomerSchemaAdapter implements SchemaBindingStrategy {

    @Autowired
    private TenantProperties tenantProperties;

    @Override
    public String getSchema(String key) {
        return tenantProperties.getSchema();
    }
}
