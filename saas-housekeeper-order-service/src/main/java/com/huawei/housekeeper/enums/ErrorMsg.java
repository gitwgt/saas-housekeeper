/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.enums;

import com.huawei.housekeeper.constants.BaseCode;

import lombok.Getter;

/**
 * 错误码枚举
 *
 * @since 2022-02-24
 */
@Getter
public enum ErrorMsg implements BaseCode {
    SKUID_ERROR(330003, "没有此项服务"),

    CUSTOMER_ERROR(330001, "用户id为空"),

    STATUS_ERROR(330002, "状态更改异常"),

    MESSAGE_ERROR(330004, "消息接收失败");

    private int code;

    private String message;

    ErrorMsg(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
