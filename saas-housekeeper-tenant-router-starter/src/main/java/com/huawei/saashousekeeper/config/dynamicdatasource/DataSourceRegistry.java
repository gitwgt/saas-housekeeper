/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.dynamicdatasource;

import com.huawei.saashousekeeper.properties.MasterSlaveProperty;

/**
 * 注册数据源,提供一个默认实现，可扩展为其他实现
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public interface DataSourceRegistry {

    /**
     * 注册数据源，更新时需要移除老的数据源，以最新配置为准
     *
     * @param key 关键字
     * @param masterSlaveProperty 主从结构数据源
     */
    void registerDataSource(String key, MasterSlaveProperty masterSlaveProperty);

    /**
     * 移除数据源并关闭连接池
     *
     * @param dataSourceGroup 需要注销的数据源
     * @throws Exception 异常
     */
    void unRegisterDataSource(DataSourceGroup dataSourceGroup) throws Exception;

    /**
     * 获取数据源， 在获取数据源时实现读写分离，负载均衡，以及事务全部走主等等
     *
     * @param key 租户信息指定的数据源
     * @return 数据源
     */
    DataSourceGroup getDataSourceGroup(String key);
}
