/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.binding;

import com.huawei.saashousekeeper.config.dynamicdatasource.DataSourceGroup;
import com.huawei.saashousekeeper.properties.DynamicSourceProperties;

import java.util.Map;

/**
 * 数据源选取适配，binding配置 > 自扩展DataSourceAdapter > defaultDataSourceAdapter
 *
 * @author lWX1156935
 * @since 2022-04-27
 */
public interface DataSourceBindingStrategy {
    /**
     * 通过关键字获取数据源组
     *
     * @param dataSourceMap 数据源
     * @return 数据源组（主从结构）
     */
    DataSourceGroup getDataSource(Map<String, DataSourceGroup> dataSourceMap, DynamicSourceProperties dynamicSourceProperties);
}
