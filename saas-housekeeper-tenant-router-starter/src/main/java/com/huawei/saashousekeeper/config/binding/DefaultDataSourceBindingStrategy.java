/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.binding;

import com.huawei.saashousekeeper.config.dynamicdatasource.DataSourceGroup;
import com.huawei.saashousekeeper.properties.DynamicSourceProperties;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import java.util.Map;

/**
 * 租户未配置绑定数据源组且未扩展DataSourceBindingStrategy时，使用此默认扩展适配器
 *
 * @author lWX1156935
 * @since 2022-04-27
 */
public class DefaultDataSourceBindingStrategy implements DataSourceBindingStrategy {
    @Override
    public DataSourceGroup getDataSource(Map<String, DataSourceGroup> dataSourceMap, DynamicSourceProperties dynamicSourceProperties) {
        String defaultSource = dynamicSourceProperties.getDefaultSource();

        // 默认配置优先， 没有默认配置时如果只配置了一个数据源
        return StringUtils.isNotBlank(defaultSource) ? dataSourceMap.get(defaultSource)
            : (dataSourceMap.size() == 1 ? dataSourceMap.entrySet().iterator().next().getValue() : null);
    }
}
