/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.config.dynamicdatasource;

import com.huawei.saashousekeeper.context.TenantContext;
import com.huawei.saashousekeeper.config.balancestrategy.LoadBalanceStrategy;
import com.huawei.saashousekeeper.constants.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 数据源组
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
@Data
@AllArgsConstructor
@Log4j2
public class DataSourceGroup {
    /**
     * 组名
     */
    private String groupName;

    /**
     * 数据源组是否开起租户schema隔离
     */
    private boolean schemaIsolationEnable;

    /**
     * 负载均衡策略
     */
    private LoadBalanceStrategy loadBalanceStrategy;

    /**
     * 主库
     */
    private List<SnapshotDataSource> masterGroup;

    /**
     * 从库
     */
    private List<SnapshotDataSource> slaveGroup;

    /**
     * 获取组
     *
     * @return 数据源组
     */
    public SnapshotDataSource getDataSource() {
        String dbStrategy = TenantContext.getDbStrategyType();
        Optional.ofNullable(masterGroup).ifPresent(master -> masterGroup = master.stream().filter(item -> !item.isClosed()).collect(Collectors.toList()));
        Optional.ofNullable(slaveGroup).ifPresent(slave ->
            slaveGroup = slave.stream().filter(item -> !item.isClosed()).collect(Collectors.toList()));
        boolean selectMaster = Constants.DB_MASTER.equalsIgnoreCase(dbStrategy) || CollectionUtils.isEmpty(slaveGroup);
        log.warn("{} select db {}", TenantContext.getDomain(), selectMaster ? Constants.DB_MASTER : Constants.DB_SLAVE);
        if (selectMaster) {
            if (CollectionUtils.isEmpty(masterGroup)) {
                log.warn("Failed to write data to the database because data source group {} does not have a valid"
                    + " primary database.", groupName);
            }
            return loadBalanceStrategy.get(masterGroup);
        }
        return loadBalanceStrategy.get(slaveGroup);
    }

    /**
     * 无效数据源组
     *
     * @return 是否有效
     */
    public boolean isInValid() {
        return (CollectionUtils.isEmpty(masterGroup) || masterGroup.stream().allMatch(SnapshotDataSource::isClosed)) && (
            CollectionUtils.isEmpty(slaveGroup) || slaveGroup.stream().allMatch(SnapshotDataSource::isClosed));
    }
}
