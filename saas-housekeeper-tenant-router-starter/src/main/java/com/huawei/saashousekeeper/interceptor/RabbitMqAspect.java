/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.interceptor;

import com.huawei.saashousekeeper.context.TenantContext;

import lombok.extern.log4j.Log4j2;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.util.Map;

/**
 * 消息切面
 *
 * @author lWX1128557
 * @since 2022-03-17
 * @deprecated 即将废弃
 */
@Aspect
@Log4j2
@Deprecated
public class RabbitMqAspect {
    /**
     * 切点
     */
    @Pointcut("@annotation(com.huawei.saashousekeeper.annotation.MqAround)")
    public void mqAroundAdvice() {
    }

    /**
     * 消息接收时做租户路由
     *
     * @param joinPoint 切点
     */
    @Around("mqAroundAdvice()")
    public void aroundAdvice(ProceedingJoinPoint joinPoint) {
        String method = joinPoint.getSignature().getName();
        // 接收消息前初始化豪猪 根据消息接收的实际场景设计，最好是从消息体中获取参数，而不是方法中获取
        Object[] args = joinPoint.getArgs();
        // 要求接受的消息头必须是 map类型，对请求头做处理
        for (Object arg : args) {
            if (arg instanceof Map) {
                Map<String, Object> headers = (Map<String, Object>) arg;
                TenantContext.setDomain((String) headers.get("tenantDomain"), true);
            }
        }
        try {
            joinPoint.proceed();
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
        } finally {
            // 方法执行后移除租户标识
            TenantContext.remove();
        }
        log.info("aspect Method: " + method);
    }
}
