/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.exception;

/**
 * 路由失败异常
 *
 * @author lWX1128557
 * @since 2022-04-19
 */
public class RoutingException extends RuntimeException {

    private static final long serialVersionUID = -8572529625761748960L;

    private int code;

    public RoutingException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public RoutingException(String message) {
        super(message);
    }

    public RoutingException(int code, String message) {
        this(code, message, null);
    }
}
