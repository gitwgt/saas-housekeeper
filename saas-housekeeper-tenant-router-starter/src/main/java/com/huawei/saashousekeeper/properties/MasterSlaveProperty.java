/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.properties;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 主从对应配置关系
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
@Data
@Accessors(chain = true)
@Log4j2
@EqualsAndHashCode(callSuper = false)
public class MasterSlaveProperty extends PoolProperties {
    /**
     * 数据源组名称，唯一
     */
    private String groupName;

    /**
     * 允许修改库连接信息，当新的连接属性与使用中的属性不一致时，使用新属性重建连接池，默认关闭
     */
    private boolean modifyEnable = false;

    /**
     * 是否生效
     */
    private boolean enable = true;

    /**
     * 数据源组是否开起租户schema隔离
     */
    private boolean schemaIsolationEnable = false;

    /**
     * 负载均衡策略
     * {@link com.huawei.saashousekeeper.config.balancestrategy.LoadBalanceStrategy}
     */
    private String loadBalanceStrategy;

    /**
     * 单主库，有需要可以改为多主
     */
    private DataSourceProperty master;

    /**
     * 多从库
     */
    private List<DataSourceProperty> slave;

    /**
     * 合并属性
     *
     * @param poolProperties 池属性
     */
    @Override
    public void mergeConfig(PoolProperties poolProperties) {
        super.mergeConfig(poolProperties);
        if (CollectionUtils.isEmpty(slave)) {
            return;
        }
        slave.stream().forEach(item -> {
            item.setDriverClassName(StringUtils.isBlank(item.getDriverClassName())
                ? master.getDriverClassName()
                : item.getDriverClassName());
            item.setUsername(StringUtils.isBlank(item.getUsername()) ? master.getUsername() : item.getUsername());
            item.setPassword(StringUtils.isBlank(item.getPassword()) ? master.getPassword() : item.getPassword());
        });
    }

    /**
     * 是否无效配置
     *
     * @return 结果
     */
    public boolean isInValid() {
        if (!enable) {
            return true;
        }
        return (master == null || !master.isEnable()) && (CollectionUtils.isEmpty(slave) || slave.stream()
            .noneMatch(item -> item.isEnable()));
    }
}
