/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.saashousekeeper.constants;

/**
 * 连接池类型
 *
 * @author lWX1156935
 * @since 2022/4/22
 */
public enum DbPoolEnum {
    /**
     * druid
     */
    POOL_DRUID("druid"),

    /**
     * Hikari
     */
    POOL_HIKARI("hikari");

    private String name;

    DbPoolEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
