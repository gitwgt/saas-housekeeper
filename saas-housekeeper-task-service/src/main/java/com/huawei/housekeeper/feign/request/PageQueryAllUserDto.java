/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.feign.request;

import com.huawei.housekeeper.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

/**
 * 用户分页查询对象
 *
 * @author cwx1109128
 * @since 2022-09-06
 */
@Data
@ApiModel("用户分页查询对象")
public class PageQueryAllUserDto extends PageRequest {
    @Length(max = 255, message = "最大长度:255")
    @ApiModelProperty(value = "用户名")
    private String userName;

    @ApiModelProperty(value = "用户角色")
    private Integer userRole;

    @ApiModelProperty(value = "用户类型")
    private Integer accountType;
}