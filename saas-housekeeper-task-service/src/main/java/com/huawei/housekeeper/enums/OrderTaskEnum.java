/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.enums;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.huawei.housekeeper.constants.TaskOrderAction;
import lombok.Getter;

import java.util.Optional;

/**
 * 订单状态
 *
 * @author lWX1128557
 * @since 2022-02-23
 */
@Getter
public enum OrderTaskEnum {

    /**
     * 待接单
     */
    WAITING(1, TaskOrderAction.TASK_ORDER_WAITING.getAction()),

    /**
     * 已完成
     */
    COMPLETED(2, TaskOrderAction.TASK_ORDER_FINISHED.getAction()),

    /**
     * 取消
     */
    CANCEL(3, TaskOrderAction.TASK_ORDER_CANCEL.getAction()),

    /**
     * 进行中
     */
    ONGOING(4, TaskOrderAction.TASK_ORDER_ACCEPT.getAction());

    private Integer status;

    private String action;

    OrderTaskEnum(Integer status, String action) {
        this.status = status;
        this.action = action;
    }


    public static Optional<OrderTaskEnum> ofAction(String action) {
        if (StringUtils.isEmpty(action)) {
            return Optional.ofNullable(null);
        }
        for (OrderTaskEnum value : OrderTaskEnum.values()) {
            if (value.getAction().equals(action)) {
                return Optional.ofNullable(value);
            }
        }
        return Optional.ofNullable(null);
    }
}
