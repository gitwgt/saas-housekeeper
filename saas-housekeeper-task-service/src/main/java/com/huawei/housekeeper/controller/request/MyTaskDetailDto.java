/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.housekeeper.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 查询个人已接任务详情
 *
 * @author jwx1116205
 * @since 2022-03-02
 */
@Setter
@Getter
@ApiModel("查询个人已接任务详情对象")
public class MyTaskDetailDto {
    @NotNull(message = "任务id必填")
    @Min(value = 1, message = "最小值:1")
    @ApiModelProperty(value = "任务id", required = true)
    private Long id;
}