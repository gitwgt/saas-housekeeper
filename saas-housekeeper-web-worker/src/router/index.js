const router = [
    {
        path: '/',
        name: '/',
        component: () => import('@/views/Login/Login.vue'),
    },
    {
        path: '/login',
        name: '/login',
        component: () => import('@/views/Login/Login.vue'),
    },
    {
        path: '/register',
        name: '/register',
        component: () => import('@/views/Register/Register.vue'),
    },
    {
        path: '/home',
        name: '/home',
        component: () => import('@/views/Home/Home.vue'),
        children: [
            {
                path: '/home/service-list',
                name: '/home/service-list',
                component: () => import('@/views/ServiceList/ServiceList.vue'),
            },
            {
                path: '/home/task-list',
                name: '/home/task-list',
                component: () => import('@/views/TaskList/TaskList.vue'),
            },
            {
                path: '/home/task-list/details',
                name: '/home/task-list/details',
                component: () => import('@/views/TaskList/TaskDetails.vue'),
            },
            {
                path: '/home/service-list/details/:id',
                name: '/home/service-list/details',
                component: () => import('@/views/ServiceList/ServiceDetails.vue'),
            },
        ],
    },
];

export default router;
