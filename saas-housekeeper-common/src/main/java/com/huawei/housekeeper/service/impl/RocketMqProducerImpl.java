package com.huawei.housekeeper.service.impl;

import com.huawei.housekeeper.entity.BaseMqMessage;
import com.huawei.housekeeper.service.RocketMqProducer;
import lombok.extern.java.Log;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;
@Log
public class RocketMqProducerImpl implements RocketMqProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 主要功能是把业务封装的消息体转化为Spring封装的消息体,这里只是实现了同步消息的推送，按企业需求可以封装不同的推送类型
     * @param destination
     * @param message
     * @param <T>
     */
    @Override
    public <T extends BaseMqMessage> void send(String destination, T message){
        Message<T> sendMessage = MessageBuilder.withPayload(message).setHeader(RocketMQHeaders.KEYS,message.getKey()).build();
        SendResult result = rocketMQTemplate.syncSend(destination,sendMessage);
        log.info("synchronized msg with key:"+message.getKey()+"has send");
    }
}
