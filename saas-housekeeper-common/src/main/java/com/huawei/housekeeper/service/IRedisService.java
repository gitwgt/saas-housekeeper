package com.huawei.housekeeper.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 对redisTemplate的封装，可在实现中保持团队的规范和一致性
 * @param <T>
 */
public interface IRedisService<T> {

    /**
     * 保存对象
     */
    void set(String key, T value, long time);

    /**
     * 保存对象
     */
    void set(String key, T value);

    /**
     * 获取属性
     */
    T get(String key);

    /**
     * 删除属性
     */
    Boolean del(String key);

    /**
     * 批量删除属性
     */
    Long del(List<String> keys);

    /**
     * 设置过期时间
     */
    Boolean expire(String key, long time);

    /**
     * 获取过期时间
     */
    Long getExpire(String key);

    /**
     * 判断是否有该对象
     */
    Boolean hasKey(String key);

    /**
     * 按delta递增
     */
    Long incr(String key, long delta);

    /**
     * 按delta递减
     */
    Long decr(String key, long delta);

    /**
     * 获取Hash结构中的对象
     */
    T hGet(String key, String hashKey);

    /**
     * 向Hash结构中放入一个对象
     */
    Boolean hSet(String key, String hashKey, T value, long time);

    /**
     * 向Hash结构中放入一个对象
     */
    void hSet(String key, String hashKey, T value);

    /**
     * 直接获取整个Hash结构
     */
    Map<Object, T> hGetAll(String key);

    /**
     * 直接设置整个Hash结构
     */
    Boolean hSetAll(String key, Map<String, T> map, long time);

    /**
     * 直接设置整个Hash结构
     */
    void hSetAll(String key, Map<String, T> map);

    /**
     * 删除Hash结构中的对象
     */
    void hDel(String key, T... hashKey);

    /**
     * 判断Hash结构中是否有该对象
     */
    Boolean hHasKey(String key, String hashKey);

    /**
     * Hash结构中对象递增
     */
    Long hIncr(String key, String hashKey, Long delta);

    /**
     * Hash结构中对象递减
     */
    Long hDecr(String key, String hashKey, Long delta);

    /**
     * 获取Set结构
     */
    Set<T> sMembers(String key);

    /**
     * 向Set结构中添加对象
     */
    Long sAdd(String key, T... values);

    /**
     * 向Set结构中添加对象
     */
    Long sAdd(String key, long time, T... values);

    /**
     * 是否为Set中的对象
     */
    Boolean sIsMember(String key, T value);

    /**
     * 获取Set结构的长度
     */
    Long sSize(String key);

    /**
     * 删除Set结构中的对象
     */
    Long sRemove(String key, T... values);

    /**
     * 获取List结构中的对象
     */
    List<T> lRange(String key, long start, long end);

    /**
     * 获取List结构的长度
     */
    Long lSize(String key);

    /**
     * 根据索引获取List中的对象
     */
    T lIndex(String key, long index);

    /**
     * 向List结构中添加对象
     */
    Long lPush(String key, T value);

    /**
     * 向List结构中添加对象
     */
    Long lPush(String key, T value, long time);

    /**
     * 向List结构中批量添加对象
     */
    Long lPushAll(String key, T... values);

    /**
     * 向List结构中批量添加对象
     */
    Long lPushAll(String key, Long time, T... values);

    /**
     * 从List结构中移除对象
     */
    Long lRemove(String key, long count, T value);
}
