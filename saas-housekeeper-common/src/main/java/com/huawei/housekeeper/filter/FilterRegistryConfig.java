
package com.huawei.housekeeper.filter;

import com.huawei.saashousekeeper.properties.ResourcesExcluderProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 功能描述
 *
 * @author lWX1128557
 * @since 2022-05-06
 */
@Configuration
public class FilterRegistryConfig {

    @Bean
    @ConditionalOnBean(ResourcesExcluderProperties.class)
    public ResloveUserInfoFilter resourcesExcluderProperties() {
        return new ResloveUserInfoFilter();
    }

    @Bean
    public CorsCommonFilter corsCommonFilter() {
        return new CorsCommonFilter();
    }
}
