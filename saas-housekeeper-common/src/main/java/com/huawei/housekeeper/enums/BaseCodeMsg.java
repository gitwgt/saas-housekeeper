/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.housekeeper.enums;
import com.huawei.housekeeper.constants.BaseCode;
import lombok.Getter;

/**
 * 状态码枚举类
 */
@Getter
public enum BaseCodeMsg implements BaseCode {
    SUCCESS(200, "success");

    private int code;

    private String message;

    BaseCodeMsg(int code, String message) {
        this.code = code;
        this.message = message;
    }
}